# Medicats

Bootstrapped with create-react-app, using React context and useReducer hook for state management.

## Running app locally

```
npm start
```

## Running test

```
npm run test
```

## Build app

```
npm run build
```

## Things to improve:

-   Render error screen when api call fails
-   Add an image component to have lazy loading, placeholder and fallback image
-   Define variables for styles like colors, width, animation time, etc
-   Add more unit tests for better coverage - only have test for sorting function at the moment
-   better UI design of course - just a simple text output at the momoent
