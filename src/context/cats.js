import React from 'react';
import catsReducer, { initialState } from '../reducers/cats';
import { getCats } from '../actions/cats';

export const CatsContext = React.createContext();

export const CatsProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(catsReducer, initialState);
    const value = {
        cats: state.cats,
        getCats: () => getCats(dispatch),
    };
    return (
        <CatsContext.Provider value={value}>{children}</CatsContext.Provider>
    );
};
