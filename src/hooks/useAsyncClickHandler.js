import { useState } from 'react';

const useAsyncClickHandler = (onClick, handleError = () => {}) => {
    const [loading, setLoading] = useState(false);
    const handleClick = async (param) => {
        setLoading(true);
        try {
            await onClick(param);
        } catch (e) {
            handleError(e);
        }
        setLoading(false);
    };
    return [loading, handleClick];
};

export default useAsyncClickHandler;
