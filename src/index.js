import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app';
import reportWebVitals from './reportWebVitals';
import { CatsProvider } from './context/cats';
import './index.scss';

ReactDOM.render(
    <React.StrictMode>
        <CatsProvider>
            <App />
        </CatsProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
