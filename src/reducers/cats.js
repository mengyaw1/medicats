import { GET_CATS_SUCCESS, GET_CATS_ERROR } from '../constants/actions';

const GENDER_MALE = 'Male';
const GENDER_FEMALE = 'Female';
const PET_CAT = 'Cat';

export const initialState = {
    cats: {},
};

export const getSortedCats = (people) => {
    const maleCats = people
        .map(
            (ppl) =>
                ppl.gender === GENDER_MALE &&
                ppl.pets &&
                ppl.pets
                    .map((pet) => pet.type === PET_CAT && pet)
                    .filter((i) => i)
        )
        .filter((i) => i)
        .flat()
        .sort((a, b) => a.name.localeCompare(b.name));
    const femaleCats = people
        .map(
            (ppl) =>
                ppl.gender === GENDER_FEMALE &&
                ppl.pets &&
                ppl.pets
                    .map((pet) => pet.type === PET_CAT && pet)
                    .filter((i) => i)
        )
        .filter((i) => i)
        .flat()
        .sort((a, b) => a.name.localeCompare(b.name));

    return {
        maleCats,
        femaleCats,
    };
};

const catsReducer = (state, action) => {
    switch (action.type) {
        case GET_CATS_SUCCESS:
            return {
                ...state,
                cats: getSortedCats(action.data),
                hasError: false,
            };
        case GET_CATS_ERROR:
            return {
                ...state,
                hasError: true,
            };
        default:
            return state;
    }
};

export default catsReducer;
