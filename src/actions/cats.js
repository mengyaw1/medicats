import { getCatsApi } from '../api/cats';
import { GET_CATS_SUCCESS, GET_CATS_ERROR } from '../constants/actions';

const getCatsSuccess = (data) => ({
    type: GET_CATS_SUCCESS,
    data,
});

const getCatsError = () => ({
    type: GET_CATS_ERROR,
});

export const getCats = async (dispatch) => {
    try {
        const response = await getCatsApi();
        // dispatch(announceMessage({ message: 'Showing all medicats' }));
        dispatch(getCatsSuccess(response.data));
    } catch (err) {
        dispatch(getCatsError());
    }
};
