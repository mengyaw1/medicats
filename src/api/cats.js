import axios from 'axios';
import { GET_CATS_URL } from '../constants/endpoints';

export const getCatsApi = async () => await axios.get(GET_CATS_URL);
