import React from 'react';
import PropTypes from 'prop-types';

import './index.scss';

export const Button = ({
    children,
    variant = 'primary',
    className = '',
    type = 'button',
    loading = false,
    visiblyDisabled = false, // button will look disabled but will remain in enabled state,
    disabled = false,
    ...props
}) => (
    <button
        {...props}
        type={type}
        className={`btn ${variant} ${className} ${
            visiblyDisabled ? 'disabled' : ''
        } ${loading ? 'loading' : ''}`}
        disabled={disabled}
    >
        <div className='btn__content'>{children}</div>
    </button>
);

Button.propTypes = {
    variant: PropTypes.oneOf(['primary', 'secondary']),
    className: PropTypes.string,
    type: PropTypes.string,
    loading: PropTypes.string,
    visiblyDisabled: PropTypes.bool,
    disabled: PropTypes.bool,
    children: PropTypes.node,
};
