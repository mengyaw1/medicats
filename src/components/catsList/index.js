import React from 'react';
import PropTypes from 'prop-types';

import './index.scss';

export const CatList = ({ cats, heading }) => (
    <div className='cat-list'>
        <div className='cat-list__heading'>{heading}</div>
        {cats &&
            cats.map((cat) => <div className='cat-list__name'>{cat.name}</div>)}
    </div>
);

CatList.propTypes = {
    cats: PropTypes.array,
    heading: PropTypes.string,
};
