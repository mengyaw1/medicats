import React from 'react';
import { Button } from './components/button';
import { CatList } from './components/catsList';
import useAsyncClickHandler from './hooks/useAsyncClickHandler';
import { CatsContext } from './context/cats';
import logo from './logo.gif';
import './app.scss';

export const App = () => {
    const { cats, getCats } = React.useContext(CatsContext);
    const handleError = () => {};
    const [viewing, handleClick] = useAsyncClickHandler(getCats, handleError);
    const showCats = cats.hasOwnProperty('maleCats');

    return (
        <div className='app'>
            <header className={`app__header ${showCats ? 'show-cats' : ''}`}>
                <img src={logo} className='home__logo' alt='logo' />
                <Button loading={viewing} onClick={handleClick}>
                    View Medicats
                </Button>
            </header>
            <body className='app__body'>
                {showCats && (
                    <>
                        <CatList cats={cats.femaleCats} heading='FEMALE' />
                        <CatList cats={cats.maleCats} heading='MALE' />
                    </>
                )}
            </body>
        </div>
    );
};
